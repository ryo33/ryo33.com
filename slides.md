---
layout: default
permalink: /slides/
---

<div class="slides">
  {% assign slides = site.slides | sort: 'date' | reverse %}
  {% for slide in slides %}
    <article class="slide">

      <h1><a href="{{ site.baseurl }}{{ slide.url }}">{{ slide.title }}</a></h1>

      <a href="{{ site.baseurl }}{{ slide.url }}" class="read-more">Read More</a>
    </article>
  {% endfor %}
</div>
