---
layout: page
---

# Domba Hihaheho

## Names

- Domba Hihaheho
- Qudzillata Hialilta Bohimayer
- Ryo33
- Ryo Hashiguchi

## Literature

- [/posts]({{ site.url }}/posts)
- [note.mu/ryo33](https://note.mu/ryo33)

## Technology

- [GitLab: ryo33](https://gitlab.com/ryo33/)
- [medium.com/@ryo33](https://medium.com/@ryo33)
- [dev.to/ryo33](https://dev.to/ryo33)

## Music
- [SoundCloud: Ryo33](https://soundcloud.com/ryo33)
- [Spotify: Ryo33 (as a listener)](https://open.spotify.com/user/22xo6muxbpfsoloyqechzh6yy)
- [Spotify: Ryo33 (as an artist)](https://open.spotify.com/artist/5eVliu0KdgjOH2IHAHy7wR)

## Social
- [Twitter: @Qudzillata](https://twitter.com/Qudzillata/)
- [Instagram: @qudzillata](https://www.instagram.com/qudzillata/)

## Slides

- [Slides]({{ site.url }}/slides)

## Merch

- [クジラータのお店](https://ryo33.booth.pm/)
